#!/bin/bash

PID_FILE="pid"

start_app() {
    if [ -f "$PID_FILE" ]; then
        echo "App is already running (PID: $(cat $PID_FILE))"
        return
    fi

    nohup ./gradlew bootRun -Dspring-boot.run.arguments=--server.port=8081,--server.address=0.0.0.0 > /dev/null 2>&1 &
    echo $! > $PID_FILE
    echo "App started with PID: $(cat $PID_FILE)"
}

stop_app() {
    if [ -f "$PID_FILE" ]; then
        PID=$(cat $PID_FILE)
        echo "Stopping app (PID: $PID)"
        kill $PID
        rm -f $PID_FILE
        echo "App stopped"
    else
        echo "App is not running"
    fi
}

restart_app() {
    if [ -f "$PID_FILE" ]; then
        stop_app
    figit st

    start_app
}

case "$1" in
    start)
        start_app
        ;;
    stop)
        stop_app
        ;;
    restart)
        restart_app
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit 0
